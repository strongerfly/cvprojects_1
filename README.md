# 计算机视觉小项目

> 请仅做个人研究使用

### 一、微信答疑、交流

<img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/imgIMG_5862.JPG?x-oss-process=style/wp" style="width:200px;" />





### 二、观看项目实际运行视频：

| 抖音                                                         | B站                                                          | 快手                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/imgIMG_5859.JPG?x-oss-process=style/wp" style="width:200px" /><br/><br />[网页版抖音](https://www.douyin.com/user/MS4wLjABAAAAPIrmWhFY-OHt5X8GZcHGqwDo3J29gYHcgG-QebKIDd4Wu_f4dwM2hNoEYyQBcim2?enter_from=search_result&enter_method=search_result&extra_params=%7B%22search_params%22%3A%7B%22search_type%22%3A%22user%22%2C%22search_id%22%3A%22202111241756340101512071374A007D0F%22%2C%22search_keyword%22%3A%22enpe%22%2C%22search_result_id%22%3A%221205502393189652%22%7D%7D) | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/imgIMG_7117.PNG?x-oss-process=style/wp" ><br>[网页版B站](https://space.bilibili.com/1355276754) | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/imgIMG_5858.JPG?x-oss-process=style/wp" style="width:200px" /><br/>[网页版快手](https://www.kuaishou.com/profile/3x54fkprp4xtu4a) |



### 三、项目列表

| 名称                                    | 截图                                                         | 方法                                     | 难度 | 代码                                                         |
| --------------------------------------- | ------------------------------------------------------------ | ---------------------------------------- | ---- | ------------------------------------------------------------ |
| 19.OCR车牌识别                          | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/img20220504215359.png?x-oss-process=style/wp" style="width:200px;" /> | 目标检测、OCR车牌识别                    | ⭐️⭐️⭐️  | [codes/19.OCR车牌识别](./codes/19.OCR车牌识别)               |
| 18.YOLOv5 + Deepsort 车辆追踪及速度分析 | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/img20220418170339.png?x-oss-process=style/wp" style="width:200px;" /> | YOLOv5、Deepsort、目标追踪、速度估计     | ⭐️⭐️⭐️⭐️ | [codes/18.deepsort道路车辆分析](./codes/18.deepsort道路车辆分析) |
| 17.YOLOv5 jetson nano 工地防护检测      | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/img20220404103032.png?x-oss-process=style/wp" style="width:200px;" /> | YOLOv5、Jetson Nano、tensoRT、Deepstream | ⭐️⭐️⭐️  | [codes/17.YOLOv5 jetson nano 工地防护检测](./codes/17.YOLOv5_jetson_nano_工地防护检测) |
| 16.树莓派口罩规范佩戴检测               | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/img20220319164803.png?x-oss-process=style/wp" style="width:200px;" /> | 树莓派、卷积神经网络、模型压缩           | ⭐️⭐️⭐️  | [codes/16.口罩规范佩戴检测](./codes/16.口罩规范佩戴检测)     |
| 15.防挡弹幕                             | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/img20220305205805.png?x-oss-process=style/wp" style="width:200px;" /> | 图像分割                                 | ⭐️⭐️⭐️  | [codes/15.防挡弹幕](./codes/15.防挡弹幕)                     |
| 14.人脸考勤机                           | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/img20220220094051.png?x-oss-process=style/wp" style="width:200px;" /> | 人脸检测与识别                           | ⭐️⭐️⭐️  | [codes/14.人脸考勤机](./codes/14.人脸考勤机)                 |
| 13.毛笔书体检测与识别                   | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/imgIMG_63991.jpeg?x-oss-process=style/wp" style="width:200px;" /> | OpenCV形态学、HOG、SVM                   | ⭐️⭐️   | [codes/13.书法书体检测与识别](./codes/13.书法书体检测与识别) |
| 11.AI分析看电视行为                     | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/imgIMG_6178.PNG?x-oss-process=style/wp"  style="width:200px;" /> | 人脸检测与识别、姿态、距离估计           | ⭐️⭐️⭐️  | [codes/11.watch_tv](./codes/11.watch_tv)                     |
| 10.AI虚拟鼠标                           | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/imgIMG_6083.PNG?x-oss-process=style/wp" style="width:200px;" /> | 手部姿态估计、静态动作                   | ⭐️    | [codes/10.virtual_mouse](./codes/10.virtual_mouse)           |
| 9.AI 虚拟点读机                         | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/img20211211154451.png?x-oss-process=style/wp" style="width:200px;" /> | 目标检测、OCR                            | ⭐️⭐️⭐️  | [codes/9.virtual reader](./codes/9.virtual%20reader)         |
| 8. 火影结印识别                         | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/img20211201102837.png?x-oss-process=style/wp" style="width:200px;" /> | 手部3D姿态估计、GCN                      | ⭐️⭐️⭐️  | [codes/8.结印识别](./codes/8.%E7%BB%93%E5%8D%B0%E8%AF%86%E5%88%AB) |
| 7.虚拟拖放 Python + opencv              | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/img20211120135236.png?x-oss-process=style/wp" style="width:200px;" /> | 手部姿态估计、静态动作                   | ⭐️⭐️   | [codes/7.virtual_drag_drop.py](./codes/7.virtual_drag_drop)  |
| 6.Python手势控制电脑音量                | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/img20211120135209.png?x-oss-process=style/wp" style="width:200px;" /> | 手部姿态估计、静态动作                   | ⭐️    | [codes/6.hand_control_volume.py](./codes/6.hand_control_volume) |
| 5.手势暂停、播放电视                    | <img src="https://enpei-md.oss-cn-hangzhou.aliyuncs.com/imgIMG_5885.jpg?x-oss-process=style/wp" style="width:200px" /> | 手部姿态估计、静态动作、Home assistant   | ⭐️⭐️⭐️  | [codes/5.hand_pause_atv/handRemote.py](./codes/5.hand_pause_atv/ ) |

